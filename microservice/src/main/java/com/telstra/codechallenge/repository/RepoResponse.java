package com.telstra.codechallenge.repository;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RepoResponse {

	@JsonProperty("items")
	private List<Repository> repositoryList;

	public List<Repository> getRepositoryList() {
		return repositoryList;
	}

	public void setRepositoryList(List<Repository> repositoryList) {
		this.repositoryList = repositoryList;
	}

}

package com.telstra.codechallenge.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class RepositoryController {

	@Autowired
	RestTemplate restTemplate;

	@Value("${git.hub.repo.url}")
	private String url;

	@Value("${git.hub.repo.params}")
	private String params;

	/**
	 * @param limit
	 * @return
	 */
	@RequestMapping(path = "/repositories", method = RequestMethod.GET)
	public List<Repository> repositories(@RequestParam(value = "limit", defaultValue = "20") int limit) {

		url = url + getDate() + params;
		List<Repository> repositories = restTemplate.getForObject(url, RepoResponse.class).getRepositoryList();
		System.out.println("repositories size:" + repositories.size());
		return repositories.stream().limit(limit).collect(Collectors.toList());
	}

	/**
	 * @return date
	 */
	private String getDate() {
		LocalDate date = LocalDate.now();
		return date.plusDays(-6).toString();
	}
}
